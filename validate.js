const fs = require('fs');
const path = require("path");
const customer = JSON.parse(fs.readFileSync(path.join(__dirname, './input_json/customer.json'), 'utf-8'));
const selected_offer_code = JSON.parse(fs.readFileSync(path.join(__dirname, './input_json/selected_offer_code.json'), 'utf-8'));
const trans_details = JSON.parse(fs.readFileSync(path.join(__dirname, './input_json/trans_details.json'), 'utf-8'));

function is_code_applicable(customer, selected_offer_code, trans_details) {
    try
    {
        let falsecase = {
            "requestId": 1,
            "CodeType": selected_offer_code.codeType,
            "ValidFor": selected_offer_code.validFor,
            "CodeName": selected_offer_code.codeName,
            "Applicable": "N",
            "msg": ``
        }
        if (!(selected_offer_code.termsFilter.channel.includes(trans_details.channel)))
        {
            let msg = `Channel ${trans_details.channel} is Not Applicable for ${selected_offer_code.codeName},  Aviailable Channels ${selected_offer_code.termsFilter.channel}`
            falsecase.msg = msg
            return falsecase
        }
        if (!(selected_offer_code.termsFilter.transTypeCode.includes(trans_details.transTypeCode)))
        {
            falsecase.msg = `Transaction Type ${trans_details.transTypeCode} is Not Applicable for ${selected_offer_code.codeName} and Available transaction types are ${selected_offer_code.termsFilter.transTypeCode}`
            return falsecase
        }
        let arr1 = customer.customerCategory
        let arr2 = selected_offer_code.termsFilter.customerCategory
        const intersection = arr1.filter(element => arr2.includes(element));
        if (!(intersection.length > 0))
        {
            falsecase.msg = `Customer Category ${customer.customerCategory} is Not Applicable for ${selected_offer_code.codeName} and  Available Customer Categories`
            return falsecase
        }
        let objdata = selected_offer_code.termsFilter.currency
        let results = objdata.map(a => a.currCode)
        if (!(results.includes(trans_details.currency)))
        {
            falsecase.msg = `Currency ${trans_details.currency} is Not Applicable for ${selected_offer_code.codeName} Available currencies are ${objdata.map(a => a.currCode)}`
            return falsecase
        }
        if (!((selected_offer_code.minMaxAmountType == 'LCY') && (parseInt(selected_offer_code.minimumINRAmount) < parseInt(trans_details.lcyAmount)) && (parseInt(trans_details.lcyAmount) < parseInt(selected_offer_code.maximumINRAmount))))
        {
            falsecase.msg = `LCY Amount ${trans_details.lcyAmount} is Not With in Range ${selected_offer_code.codeName}, Range is from ${selected_offer_code.minimumINRAmount} to ${selected_offer_code.maximumINRAmount}`
            return falsecase
        }
        if (!((selected_offer_code.minMaxAmountType == 'LCY') && (parseInt(selected_offer_code.minimumINRAmount) < parseInt(trans_details.lcyAmount)) && (parseInt(trans_details.lcyAmount) < parseInt(selected_offer_code.maximumINRAmount))))
        {
            falsecase.msg = `FCY Amount ${trans_details.lcyAmount} is Not With in Range ${selected_offer_code.codeName}, Range is from ${selected_offer_code.minimumINRAmount} to ${selected_offer_code.maximumINRAmount}`
            return falsecase
        }
        let date = new Date(trans_details.transDate)
        let startdate = new Date(selected_offer_code.startDateTime)
        let enddate = new Date(selected_offer_code.endDateTime)
        if (!((startdate < date) && (date < enddate)))
        {
            falsecase.msg = `Transaction Date ${trans_details.transDate} is Not With in Range ${selected_offer_code.codeName}, Range is From ${selected_offer_code.startDateTime} to ${selected_offer_code.endDateTime}`
            return falsecase
        }
        else
        {
            let applicable = "Y"
            falsecase.Applicable = applicable
            return falsecase
        }
    } catch (err)
    {
        return { "status": "unsuccess", "msg": err.message, "applicable": "N" };
    }
}
console.log(is_code_applicable(customer, selected_offer_code, trans_details));
module.exports = is_code_applicable;