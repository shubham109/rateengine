const { assert } = require('chai');
const is_code_applicable = require('./validate');
const fs = require('fs');
const path = require('path');
const customer = JSON.parse(fs.readFileSync(path.join(__dirname, './input_json/customer.json'), 'utf-8'));
const selected_offer_code = JSON.parse(fs.readFileSync(path.join(__dirname, './input_json/selected_offer_code.json'), 'utf-8'));
const trans_details = JSON.parse(fs.readFileSync(path.join(__dirname, './input_json/trans_details.json'), 'utf-8'));
describe('Offer details Testing', function () {
    it('should return discount detail', async function () {
        const mainFunction = await is_code_applicable(customer, selected_offer_code, trans_details);
        assert.doesNotThrow(() => mainFunction);
        assert.isObject(mainFunction)
        assert.exists(mainFunction, { "Applicable": "Y" })
    })
});